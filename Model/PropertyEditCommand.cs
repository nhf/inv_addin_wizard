﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Interop;
using Nhf.Inv.AddIn.Wizard.Properties;
using Nhf.Inv.AddIn.Wizard.View;
using Nhf.Inv.AddIn.Wizard.ViewModel;
using Inv.Common.Adapter;
using Inv.Common.Adapter.InventorAdapters;
using Inv.Common._Core;
using Inv.Common._Core.Extensions;
using Inventor;
using Application = Inventor.Application;
using Environment = System.Environment;
using IPictureDisp = stdole.IPictureDisp;
using Edison;


using Inv.Common._Core.Adn;
using Inv.Common._Core.Base;
using Edison.Wrapper;

#endregion

namespace Nhf.Inv.AddIn.Wizard
{

    internal class PropertyEditCommand : AdnButtonCommandBase
    {
        public PropertyEditCommand(Application application, BaseAddInServer addIn) : base(application)
        {
            ClientId = addIn.AddInClientId();
        }

        #region Property Overrides

        public override string DisplayName => _displayName;

        private const string _displayName = "Property Wizard";

        public override string InternalName => _internalName;
        
        private const string _internalName = "Nhf.Inv.AddIn.Wizard.Command.Edit";

        public override CommandTypesEnum Classification => CommandTypesEnum.kNonShapeEditCmdType;

        public override string Description => "A simplified interface to setting iProperties";

        public override string ToolTipText => Description;

        public override ButtonDisplayEnum ButtonDisp => ButtonDisplayEnum.kDisplayTextInLearningMode;

        protected override Icon StandardIcon => new Icon(Resources.ic_tune_black, 16, 16);

        protected override Icon LargeIcon => new Icon(Resources.ic_tune_black, 32, 32);

        protected override void OnExecute(NameValueMap context)
        {
            // Launch new Property Wizard instance
            var pwv = new PropertyWizardViewWpf(new PropertyWizardViewModel(Bootstrap.Backdoor(InvApp)));
            new WindowInteropHelper(pwv).Owner = new IntPtr(InvApp.MainFrameHWND);
            pwv.ShowDialog();
            Terminate();
        }

        protected override void OnHelp(NameValueMap context) { }

        #endregion

        public static void Execute(Application invApp, bool synchronous)
        {
            Execute(invApp, _internalName, synchronous);
        }
    }
}