﻿using Nhf.Inv.AddIn.Wizard.Properties;
using Inv.Common.R;
using Inv.Common._Core.Extensions;
using Inventor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Nhf.Inv.Common._Core;
using WPFCustomMessageBox;

namespace Nhf.Inv.AddIn.Wizard.Model
{
    internal sealed class WizardApplicationEvents
    {
        #region Application and Document Events

        public static void AddApplicationEvents(ApplicationEvents applicationEvents)
        {
            // Add OnSave hooks
            applicationEvents.OnSaveDocument += onSave_AutoDim;
            applicationEvents.OnSaveDocument += onSave_ValidateProperties;
        }

        public static void RemoveApplicationEvents(ApplicationEvents applicationEvents)
        {
            // Remove OnSave hooks
            applicationEvents.OnSaveDocument -= onSave_AutoDim;
            applicationEvents.OnSaveDocument -= onSave_ValidateProperties;
        }

        private static void onSave_AutoDim(_Document doc, EventTimingEnum timing, NameValueMap context,
            out HandlingCodeEnum handled)
        {
            handled = HandlingCodeEnum.kEventNotHandled;
            if (timing == EventTimingEnum.kAfter ||
                !Settings.Default.AutoDim_Enabled ||
                !(doc.IsAssembly() || doc.IsPart())) return;

            string[] xyz = Settings.Default.AutoDim_UseCustomNames ? new[] {
                    Settings.Default.AutoDim_X,
                    Settings.Default.AutoDim_Y,
                    Settings.Default.AutoDim_Z
            } : null;

            doc.AutoDim(xyz, Settings.Default.AutoDim_UnfoldSheetMetal);

            handled = HandlingCodeEnum.kEventHandled;
        }

        /// <summary>
        ///     OnSaveDocument event to handle validating and fixing iProperties.
        /// </summary>
        /// <param name="doc">The document being saved</param>
        /// <param name="timing">Timing of this call, either kBefore or kAfter the Save</param>
        /// <param name="context">
        ///     NVM containing either SaveCopyAsFilename or SaveFileName, and TopLevelSaveFileName.
        ///     TopLevelSaveFileName will always be provided. A value of "" means the part is being saved for the first time.
        ///     If the part being saved is nested in an assembly, the value will be that of the top level assembly. If this is 
        ///     a save-copy-as operation, the value will be the file name of the original file. Otherwise,
        ///     it is just the file name.
        /// </param>
        /// <param name="handled">
        ///     Handling status, must be set to kEventHandled, kEventNotHandled, or kEventAborted - which will abort this save.
        /// </param>
        private static void onSave_ValidateProperties(_Document doc, EventTimingEnum timing, NameValueMap context,
            out HandlingCodeEnum handled)
        {
            handled = HandlingCodeEnum.kEventNotHandled;

#if VAULT3
            // This runs the Prop Wizard during the Save process if the part is new

            string topFileName = null;
            try
            {
                 topFileName = context.Value[Constant.nvmKey_SaveContext_TopLevelSaveFileName] as string;
            }
            catch
            {
                // Squash
            }

            string saveCopyAsFileName = null;
            try
            {
                 saveCopyAsFileName = context.Value[Constant.nvmKey_SaveContext_SaveCopyAsFileName] as string;
            }
            catch
            {
                // Squash
            }

            if (timing == EventTimingEnum.kBefore &&
                (string.IsNullOrEmpty(topFileName) || !string.IsNullOrEmpty(saveCopyAsFileName)))
            {
                PropertyEditCommand.Execute((Inventor.Application)doc?.GetComponentDefinition()?.Application, true);
            }
#endif

            if (timing == EventTimingEnum.kAfter || !doc.Dirty || 
                !Settings.Default.PropertyWizard_ValidateProperties ||
                !(doc.IsAssembly() || doc.IsPart())) return;

            List<string> errors = PropertyValidator.Validate(doc, unfoldSheet: Settings.Default.AutoDim_UnfoldSheetMetal, fixPartNum: Settings.Default.PropertyWizard_FixPartNum);

            // Done validating
            // Display any inconsistencies that were discovered

            if (!Settings.Default.ShowErrors || !Settings.Default.ShowErrorsThisSession) return;

            if (errors.Count > 0)
            {
                errors.Sort();

                string yesStr = "Continue";
                string noStr = "Stop showing these";
                string cancelStr = "Fix now";

                MessageBoxResult mbr = CustomMessageBox.ShowYesNoCancel(string.Join(System.Environment.NewLine, errors), "", yesStr, noStr, cancelStr);
                
                if (mbr == MessageBoxResult.No)
                {
                    Settings.Default.ShowErrorsThisSession = false;
                }

                if (mbr == MessageBoxResult.Cancel)
                {
                    PropertyEditCommand.Execute((Inventor.Application)doc?.GetComponentDefinition()?.Application, true);
                }
            }
        }

        #endregion
    }
}
