﻿#region

using System;
using Inv.Common.Adapter;
using Inv.Common.Adapter.InventorAdapters;
using Inv.Common._Core;
using Inv.Common._Core.Extensions;
using Inventor;
using Property = Inv.Common.R.Property;
using Inv.Common._Core.Interface;
using Edison.Interface;
using Inv.Common._Core.Util;
using Inv.Common.R;

#endregion

namespace Nhf.Inv.AddIn.Wizard
{
    public static class PropertyDefinitions
    {

        private static string ReadPartNumber(IDocument doc, IDataSet designProps = null)
        {
            if (designProps == null)
            {
                designProps = new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsDesign);
            }

            string partNum = designProps[Property.KeyPartNum] as string;

            // Test if the iProperty part number is either empty, or the default Inventor file name containing 
            // "Part" or "Assembly" (which may indicate the part has not yet been saved).
            // In either case we should attempt to parse the file name to extract the part number.
            if (string.IsNullOrEmpty(partNum) || partNum.ToUpper().Contains(new[] { "PART", "ASSEMBLY" }).Length > 0)
            {
                partNum = System.IO.Path.GetFileNameWithoutExtension(doc.FileName);
            }
            return partNum;
        }

        private static ClassId ReadClass(IDocument doc, IDataSet customProps = null)
        {
            ClassId classId = ClassId.None;
            if (customProps == null)
            {
                customProps = new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsCustom);
            }

            string classIdStr = customProps[Property.KeyClassId] as string;
            if (string.IsNullOrEmpty(classIdStr) || classIdStr == "None")
            {
                classId = NhfComponent.GetClassId(doc.FileName);
            } 
            else
            {
                classId = EnumAttrUtil.GetEnumFromDescription(classIdStr, classId);
            }

            return classId;
        }

        private static string ReadProject(IDocument doc, IDataSet designProps = null, ClassId classId = ClassId.None)
        {
            if (designProps == null)
            {
                new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsDesign);
            }

            string project = designProps[Property.KeyProject] as string;
            if (string.IsNullOrEmpty(project))
            {
                project = NhfComponent.GetProjectNumber(doc.FileName, classId);
            }
            return project;
        }

        private static string ReadMfgMethod(IDocument doc, IDataSet customProps = null)
        {
            if (customProps == null)
            {
                customProps = new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsCustom);
            }

            string mfg = customProps[Property.KeyTypeCode] as string;
            if (string.IsNullOrEmpty(mfg))
            {
                // TODO: set default MfgMethod based on Class
            }
            return mfg;
        }

        private static AnalysisCode ReadAnalysisCode(IDocument doc, IDataSet customProps = null)
        {
            AnalysisCode ac = AnalysisCode.None;
            if (customProps == null)
            {
                customProps = new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsCustom);
            }

            string acStr = customProps[Property.KeyAnalysisCode] as string;
            if (!string.IsNullOrEmpty(acStr))
            {
                try
                {
                    ac = (AnalysisCode)Convert.ToInt32(acStr);
                }
                catch
                {
                    // Squash
                }
            }

            return ac;
        }

        public static void ReadAll(IDocument doc, INhfPropertiesContainer dest)
        {
            IDataSet _documentPropertySetAdapter = new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsDocument);
            IDataSet _designPropertySetAdapter = new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsDesign);
            IDataSet _customPropertySetAdapter = new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsCustom);
            IDataSet _userParamWrapper = new UserParameterGroupWrapper(doc.Underlying());

            dest.PartNum = ReadPartNumber(doc, _customPropertySetAdapter);

            ClassId classId = ReadClass(doc, _customPropertySetAdapter);
            dest.SelectedPartClass = EnumAttrUtil.Description(classId);

            dest.ProjectNum = ReadProject(doc, _designPropertySetAdapter, classId);

            dest.MfgMethod = ReadMfgMethod(doc, _customPropertySetAdapter);

            AnalysisCode ac = ReadAnalysisCode(doc, _customPropertySetAdapter);
            dest.AnalysisCode = EnumAttrUtil.Description(ac);

            dest.BarNum = _customPropertySetAdapter[Property.KeyBarNumber] as string;

            dest.Description = _designPropertySetAdapter[Property.KeyDescription] as string;

            dest.Alloy = _customPropertySetAdapter[Property.KeyAlloy] as string;

            dest.Finish = _customPropertySetAdapter[Property.KeyFinish] as string;

            if (_customPropertySetAdapter.Contains(Property.KeyHeight) || _customPropertySetAdapter.Contains(Property.KeyWidth))
            {
                dest.Length = (_customPropertySetAdapter.Get(Property.KeyWidth) as Inventor.Property)?.Expression;
                dest.Is2D = true;
            }
            else
            {
                dest.Length = (_customPropertySetAdapter.Get(Property.KeyLength) as Inventor.Property)?.Expression;
            }

            dest.Height = (_customPropertySetAdapter.Get(Property.KeyHeight) as Inventor.Property)?.Expression;
        }

        public static void WriteAll(INhfPropertiesContainer source, IDocument doc, IDataSet _designPropertySetAdapter = null, IDataSet _customPropertySetAdapter = null)
        {
            _designPropertySetAdapter = _designPropertySetAdapter ?? new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsDesign);
            _customPropertySetAdapter = _customPropertySetAdapter ?? new PropertySetAdapter(doc.Underlying(), PropertySetAdapter.PropertysetsCustom);

            if (source.PartNum != null)
            {
                _designPropertySetAdapter[Property.KeyPartNum] = source.PartNum;
            }

            ClassId partClass;
            if (!Enum.TryParse(source.SelectedPartClass, out partClass))
            {
                partClass = ClassId.None;
            }
            _customPropertySetAdapter[Property.KeyClassId] = EnumAttrUtil.Description(partClass);

            if (source.ProjectNum != null)
            {
                _designPropertySetAdapter[Property.KeyProject] = source.ProjectNum;
            }

            if (source.BarNum != null)
            {
                switch (partClass)
                {
                    case ClassId.EP:
                        if (source.MfgMethod != null && source.MfgMethod == "M")
                        {
                            goto case ClassId.EX;
                        }
                        break;
                    case ClassId.AA:
                    case ClassId.EX:
                        _customPropertySetAdapter[Property.KeyBarNumber] = source.BarNum;
                        break;
                }
            }

            if (source.Description != null)
            {
                _designPropertySetAdapter[Property.KeyDescription] = source.Description.ToUpper();
            }
            if (source.Alloy != null)
            {
                _customPropertySetAdapter[Property.KeyAlloy] = source.Alloy;
            }
            if (source.Finish != null)
            {
                _customPropertySetAdapter[Property.KeyFinish] = source.Finish;
            }

            // Write the dimensions
            if (source.Is2D)
            {
                _customPropertySetAdapter.Delete(Property.KeyLength);

                if (source.Height != null)
                {
                    _customPropertySetAdapter[Property.KeyHeight] = source.Height;
                }
            }
            else
            {
                _customPropertySetAdapter.Delete(Property.KeyHeight);
                _customPropertySetAdapter.Delete(Property.KeyWidth);
            }

            if (source.Length != null)
            {
                _customPropertySetAdapter[source.Is2D ? Property.KeyWidth : Property.KeyLength] = source.Length;
            }

            if (source.MfgMethod != null)
            {
                _customPropertySetAdapter[Property.KeyTypeCode] = source.MfgMethod;
                SetViewAsAsm(_customPropertySetAdapter, source.MfgMethod);
            }

            AnalysisCode ac = AnalysisCode.None;

            string acStr = source.AnalysisCode;
            if (!string.IsNullOrEmpty(acStr))
            {
                ac = EnumAttrUtil.GetEnumFromDescription(acStr, ac);
            }

            _customPropertySetAdapter[Property.KeyAnalysisCode] = Convert.ToString((int)ac);
            // TODO: remove reduntant property here once the Vault tooling gets updated
            _customPropertySetAdapter[Property.KeyCostCode] = Convert.ToString((int)ac);

            /*
            if (MCode != null)
            {
                //_userParamWrapper[Broker.Key_ExchangeMCode] = MCode;
            }
            */

            SetBounds(doc.Underlying(), _customPropertySetAdapter);
            PropertyDefinitions.SetClassDependantProperties(doc, _customPropertySetAdapter, partClass);
        } 

#if VAULT2
        private static string MakeEpicorBaseDescription(bool isMetal)
        {
            return isMetal ? Property.ValEpicorDescMetal : Property.ValEpicorDesc;
        }

        private static string MakeEpicorDescription(bool is2D, bool isMetal)
        {
            string baseDesc = MakeEpicorBaseDescription(isMetal);

            if (is2D)
            {
                baseDesc += Property.ValEpicorDescDims2D;
            }
            else
            {
                baseDesc += Property.ValEpicorDescDims1D;
            }
            return baseDesc;
        }
#endif

        private static void SetBounds(Document doc, IDataSet customPropertySetAdapter = null)
        {
            if (customPropertySetAdapter == null)
            {
                customPropertySetAdapter = new PropertySetAdapter(doc, PropertySetAdapter.PropertysetsCustom);
            }

            decimal[] dims = doc.Dimensions();
            customPropertySetAdapter[DocumentExtensions.X] = (double) dims[0];
            customPropertySetAdapter[DocumentExtensions.Y] = (double) dims[1];
            customPropertySetAdapter[DocumentExtensions.Z] = (double) dims[2];
        }

        public static void SetClassDependantProperties(IDocument doc, IDataSet customProps = null, ClassId classId = ClassId.None)
        {
            customProps = customProps ?? new PropertySetAdapter(doc, PropertySetAdapter.PropertysetsCustom);

            if (classId == ClassId.None)
            {
                Enum.TryParse((string)customProps[Property.KeyClassId], out classId);
            }

            if (classId != ClassId.None)
            {
                SetApproved(customProps, classId);
                SetCompany(customProps, classId);
                SetCostMethod(customProps, classId);
                SetEcoGroupId(customProps, classId);
                SetPlant(customProps, classId);
                SetQtyBearing(customProps, classId);
                SetRelatedOperation(customProps, classId);
                SetUpdatePartPlant(customProps, classId);

                SetLotMgfBatch(customProps, classId);
                SetLotExpDt(customProps, classId);
                SetMtlPartNum(customProps, classId);
                SetTrackLots(customProps, classId);
                SetTrackSerialNum(customProps, classId);
                // SetTypeCode(customProps, classId);
                SetUomClassId(customProps, classId);
                // SetViewAsAsm(customProps, classId);

#if VAULT2
                SetEpicorDescription(doc, customProps, classId);
#endif
            }
        }

#if VAULT2
        private static void SetEpicorDescription(IDocument doc, IDataSet customProps, ClassId classId)
        {
            var isMetal = false;

            switch (classId)
            {
                case ClassId.AA:
                case ClassId.AM:
                case ClassId.AS:
                case ClassId.BA:
                case ClassId.BM:
                case ClassId.BS:
                case ClassId.CA:
                case ClassId.EX:
                    isMetal = true;
                    break;
            }

            bool is2D = doc.Underlying().IsSheetMetal();

            switch (classId)
            {
                case ClassId.GL:
                case ClassId.IN:
                case ClassId.FG:
                case ClassId.ST:
                    is2D = true;
                    break;
            }

            if (customProps.Contains(Property.KeyWidth))
            {
                is2D = true;
            }

            string epicorDesc = MakeEpicorDescription(is2D, isMetal);
            customProps[Property.KeyEpicorDesc] = epicorDesc;
        }
#endif

        private static void SetApproved(IDataSet customProps, ClassId classId)
        {
            customProps[Property.KeyApproved] = "FALSE"; // Always
        }

        private static void SetCompany(IDataSet summaryProps, ClassId classId)
        {
            if (string.IsNullOrEmpty((string) summaryProps[Property.KeyCompany]))
            {
                summaryProps[Property.KeyCompany] = "NHF01";
            }
        }

        private static void SetCostMethod(IDataSet customProps, ClassId classId)
        {
            customProps[Property.KeyCostMethod] = "A";
        }

#if VAULT2
        private static void SetEcoGroupId(IDataSet customProps, ClassId classId)
        {
            customProps[Property.KeyEcoGroupId] = "DJO";
        }
#endif
#if VAULT3
        private static void SetEcoGroupId(IDataSet customProps, ClassId classId)
        {
            customProps[Property.KeyEcoGroupId] = "VAULT IMPORT";
        }
#endif

        private static void SetPlant(IDataSet customProps, ClassId classId)
        {
            customProps[Property.KeyPlant] = "MFGSYS";
        }

        private static void SetQtyBearing(IDataSet customProps, ClassId classId)
        {
            customProps[Property.KeyQtyBearing] = "TRUE";
        }

        private static void SetRelatedOperation(IDataSet customProps, ClassId classId)
        {
            customProps[Property.KeyRelatedOperation] = "10";
        }

        private static void SetUpdatePartPlant(IDataSet customProps, ClassId classId)
        {
            customProps[Property.KeyUpdatePartPlant] = "TRUE";
        }

        private static void SetLotMgfBatch(IDataSet customProps, ClassId classId)
        {
            if (classId == ClassId.EX) return;
            customProps[Property.KeyLotMfgBatch] = "FALSE";
        }

        private static void SetLotExpDt(IDataSet customProps, ClassId classId)
        {
#if VAULT3
            switch (classId)
            {
                case ClassId.GZ:
                    customProps[Property.KeyLotExpDate] = "YES";
                    break;
                case ClassId.TF:
                    customProps[Property.KeyLotExpDate] = "FALSE";
                    break;
            }
#endif
        }

        private static void SetMtlPartNum(IDataSet customProps, ClassId classId)
        {
            string barNumber = (string) customProps[Property.KeyBarNumber];
            if (string.IsNullOrEmpty(barNumber)) return;
            switch (classId)
            {
                case ClassId.AA:
                case ClassId.EX:
                    customProps[Property.KeyMtlPartNum] = customProps[Property.KeyBarNumber];
                    break;
                case ClassId.EP:
                    if ("M" == (string) customProps[Property.KeyTypeCode])
                    {
                        customProps[Property.KeyMtlPartNum] = customProps[Property.KeyBarNumber];
                    }
                    break;
                default:
                    customProps.Delete(Property.KeyBarNumber);
                    break;
            }
        }

        private static void SetTrackLots(IDataSet customProps, ClassId classId)
        {
            switch (classId)
            {
                case ClassId.GZ:
                    customProps[Property.KeyTrackLots] = "TRUE";
                    break;
                default:
                    customProps[Property.KeyTrackLots] = "FALSE";
                    break;
            }
        }

        private static void SetTrackSerialNum(IDataSet customProps, ClassId classId)
        {
            switch (classId)
            {
                case ClassId.UA:
                    customProps[Property.KeyTrackSerialNum] = "TRUE";
                    break;
                default:
                    customProps[Property.KeyTrackSerialNum] = "FALSE";
                    break;
            }
        }

        /*
         * This shouldn't exist - MfgMethod is set by the user in the form.
        
        private static void SetTypeCode(IDataSet customProps, ClassId classId)
        {
            switch (classId)
            {
                case ClassId.EP:
                case ClassId.SA:
                    break;
                case ClassId.AA:
                case ClassId.EX:
                case ClassId.UA:
                    customProps[Property.KeyTypeCode] = "M";
                    break;
                default:
                    customProps[Property.KeyTypeCode] = "P";
                    break;
            }
        }
        */

        private static void SetUomClassId(IDataSet customProps, ClassId classId)
        {
            switch (classId)
            {
                case ClassId.EG:
                    customProps[Property.KeyUomClassId] = "LENGTH";
                    break;
                case ClassId.EP:
                    if ("M" == (string) customProps[Property.KeyTypeCode])
                    {
                        // TODO
                    }
                    goto default;
                case ClassId.TF:
                    // TODO
                    goto default;
                case ClassId.GZ:
                    customProps[Property.KeyUomClassId] = "SILICONE";
                    break;
                default:
                    customProps[Property.KeyUomClassId] = "COUNT";
                    break;
            }
        }

        private static void SetViewAsAsm(IDataSet customProps, string mfgCode)
        {
            switch (mfgCode)
            {
                case "M":
                    customProps[Property.KeyViewAsAsm] = "TRUE";
                    break;
                default:
                    customProps[Property.KeyViewAsAsm] = "FALSE";
                    break;
            }
        }
    }
}