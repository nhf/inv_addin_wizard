#region

using System;
using System.Reflection;
using System.Runtime.InteropServices;
using Nhf.Inv.AddIn.Wizard.Model;
using Nhf.Inv.AddIn.Wizard.Properties;
using Inv.Common.Adapter.InventorAdapters;
using Inventor;
using Inv.Common._Core.Base;
using Inv.Common._Core.Adn;

#endregion

namespace Nhf.Inv.AddIn.Wizard
{
    /// <summary>
    ///     This is the primary AddIn Server class that implements the ApplicationAddInServer interface
    ///     that all Inventor AddIns are required to implement. The communication between Inventor and
    ///     the AddIn is via the methods on this interface.
    /// </summary>
    [Guid("3743df95-fbe9-4792-af83-09d7bcfdb027")]
    public class WizardAddInServer : BaseAddInServer
    {
        // ReSharper disable once EmptyConstructor
        public WizardAddInServer()
        {
            // Default constructor for Inventor to instantiate
        }


#region ApplicationAddInServer Members

        public override void OnActivate()
        {
            commands.Add(new PropertyEditCommand(invApp, this));
            WizardApplicationEvents.AddApplicationEvents(invApp.ApplicationEvents);
        }

        public override void OnDeactivate()
        {
            // This method is called by Inventor when the AddIn is unloaded.
            // The AddIn will be unloaded either manually by the user or
            // when the Inventor session is terminated

            // Write out the cfg in case any changes are being held in memory
            if (Settings.Default.ShowErrorsThisSession == false)
            {
                Settings.Default.ShowErrorsThisSession = true;
            }

            Settings.Default.Save();

            // Remove custom application events
            WizardApplicationEvents.RemoveApplicationEvents(invApp.ApplicationEvents);
        }
    }

#endregion
}