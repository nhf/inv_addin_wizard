Changes/fixes in this build
Added cost code drop down in manufacturing section - options change per part class
Enabled button in Assemblies
Removed Company field
Descriptions are transformed to ALL CAPS
Added BoM structure selection drop down