﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows.Input;
using Nhf.Inv.AddIn.Wizard.Properties;
using Inv.Common.Adapter.InventorAdapters;
using Inv.Common._Core;
using Inv.Common._Core.Extensions;
using Property = Inv.Common.R.Property;
using Inv.Common.R;
using System.ComponentModel;
using Inv.Common._Core.Interface;
using Edison.Interface;
using Inv.Common._Core.Util;

#endregion

namespace Nhf.Inv.AddIn.Wizard.ViewModel
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class PropertyWizardViewModel : INotifyPropertyChanged, INhfPropertiesContainer
    {
        private readonly IApplication _inv;
        private readonly IDocument _doc;

        // Alloy
        private string _alloy;

        // Bar Num
        private string _barNum;

        // Description
        private string _desc;

        // Finish
        private string _finish;

        // Length
        private string _length;

        // Manufacturing company
        private string _mcode;

        private string[] _mcodes;

        // Manufacturing method
        private string _mfg;

        // Class Id
        private string _partClass;

        // Part Number
        private string _partNum;

        // Project Num
        private string _projectNum;

        // Width
        private string _height;

        public event PropertyChangedEventHandler PropertyChanged;

        public PropertyWizardViewModel(IApplication invApp)
        {
            _inv = invApp;
            _doc = _inv.ActiveDocument;

            Save = new CommandHandler(save, true);
            Cancel = new CommandHandler(close, true);

            readProperties();
        }

        public ICommand Save { get; }
        public ICommand Cancel { get; }

        public Action CloseAction { private get; set; }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public string PartNum
        {
            get { return _partNum; }
            set { _partNum = value; }
        }

        public bool IsEX { get; set; }

        public string SelectedPartClass
        {
            get
            {
                return _partClass;
            }
            set
            {
                _partClass = value;
                IsEX = _partClass == EnumAttrUtil.Description(ClassId.EX);
                onSelectedPartClassChanged();
            }
        }

        private void onSelectedPartClassChanged()
        {
            OnPropertyChanged("SelectedPartClass");
            OnPropertyChanged("AnalysisCodeOptions");
        }

        public IEnumerable<ClassId> PartClassIds => Enum.GetValues(typeof(ClassId)).Cast<ClassId>();

        public string ProjectNum
        {
            get { return _projectNum; }
            set { _projectNum = value; }
        }

        public string BarNum
        {
            get { return _barNum; }
            set { _barNum = value; }
        }

        public string Description
        {
            get { return _desc; }
            set { _desc = value; }
        }

        public string Alloy
        {
            get { return _alloy; }
            set { _alloy = value; }
        }

        public string[] Alloys => new[]
        {
            "3003-H14",
            "304",
            "316",
            "5005-H34",
            "5052",
            "6005-T5",
            "6061-T6",
            "6063-T52",
            "6063-T6",
            "A36",
            "A500 (Grade B)",
            "A529 (Grade 50)",
            "A653",
            "A992",
            "Composite"
        };

        public string Finish
        {
            get { return _finish; }
            set { _finish = value; }
        }

        public string[] Finishes => new[]
        {
            "MTL-1",
            "MTL-2",
            "MTL-3",
            "MTL-4",
            "MTL-5",
            "MTL-6",
            "MTL-7",
            "MTL-8",
            "MTL-9",
            "RAW"
        };

        public string Length
        {
            get { return _length; }
            set { _length = value; }
        }

        public string Height
        {
            get { return _height; }
            set { _height = value; }
        }

        private bool _is2D;

        public bool Is2D
        {
            get { return _is2D; }
            set
            {
                _is2D = value;
                OnPropertyChanged("Is2D");

                if (!_is2D)
                {
                    Height = null;
                }
            }
        }

        public string[] AutoDims => new[]
        {
            "=<" + (Settings.Default.AutoDim_UseCustomNames ? Settings.Default.AutoDim_X : DocumentExtensions.X) + ">",
            "=<" + (Settings.Default.AutoDim_UseCustomNames ? Settings.Default.AutoDim_Y : DocumentExtensions.Y) + ">",
            "=<" + (Settings.Default.AutoDim_UseCustomNames ? Settings.Default.AutoDim_Z : DocumentExtensions.Z) + ">"
        };

        public string MfgMethod
        {
            get { return _mfg; }
            set { _mfg = value; }
        }

        public string[] MfgMethods => new[] {"M", "P"};

        // Manufacturing company
        private string _analysisCode { get; set; }

        public string AnalysisCode
        {
            get
            {
                return _analysisCode;
            }
            set
            {
                _analysisCode = value;
            }
        }

        private AnalysisCode[] _analysisCodeOptions;

        public string[] AnalysisCodeOptions
        {
            get
            {
                _analysisCodeOptions = AnalysisCodes.AllowedAnalysisCodesFor(SelectedPartClass);
                return _analysisCodeOptions.Select(ac => EnumAttrUtil.Description(ac)).ToArray();
            }
        }

        public string SelectedBomStructure
        {
            get
            {
                return BomStructures.FromInventor(_doc.Underlying().GetComponentDefinition().BOMStructure).Name();
            }
            set
            {
                BomStructure bs = BomStructure.Default;
                if (Enum.TryParse(value, out bs))
                {
                    _doc.Underlying().GetComponentDefinition().BOMStructure = bs.InventorBOMStructure();
                }
            }
        }

        public string[] BomStructuresOptions
        {
            get { return Enum.GetNames(typeof(BomStructure)); }
        }

        /*
        public string SelectedBomUnit
        {
            get
            {
                return "";
            }
        }

        public string[] BomUnitOptions
        {
            get { return Enum.GetNames(typeof(BomUnit)); }
        }
        */

        private void save()
        {
            writeProperties();
            close();
        }

        private void close()
        {
            CloseAction?.Invoke();
        }

        private void readProperties()
        {
            PropertyDefinitions.ReadAll(_doc, this);
        }

        // Write outs
        private void writeProperties()
        {
            PropertyDefinitions.WriteAll(this, _doc);
        }
    }
}