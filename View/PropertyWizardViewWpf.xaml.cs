﻿#region

using System.Windows;
using Nhf.Inv.AddIn.Wizard.ViewModel;

#endregion

namespace Nhf.Inv.AddIn.Wizard.View
{
    /// <summary>
    ///     Interaction logic for PropertyWizardViewWpf.xaml
    /// </summary>
    public partial class PropertyWizardViewWpf : Window
    {
        public PropertyWizardViewWpf()
        {
            InitializeComponent();
        }

        public PropertyWizardViewWpf(PropertyWizardViewModel viewModel)
        {
            InitializeComponent();
            viewModel.CloseAction = Close;
            DataContext = viewModel;
        }
    }
}