# README 

Unified interface to iProperties that previously were all manually entered. Based on inputs in this form, several properties are also set automatically and never shown to the user. Also contains logic to get Part dimensions.