﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nhf.Inv.AddIn.Wizard.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AutoDim_UseCustomNames {
            get {
                return ((bool)(this["AutoDim_UseCustomNames"]));
            }
            set {
                this["AutoDim_UseCustomNames"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool AutoDim_Enabled {
            get {
                return ((bool)(this["AutoDim_Enabled"]));
            }
            set {
                this["AutoDim_Enabled"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool AutoDim_UnfoldSheetMetal {
            get {
                return ((bool)(this["AutoDim_UnfoldSheetMetal"]));
            }
            set {
                this["AutoDim_UnfoldSheetMetal"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("_X")]
        public string AutoDim_X {
            get {
                return ((string)(this["AutoDim_X"]));
            }
            set {
                this["AutoDim_X"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("_Y")]
        public string AutoDim_Y {
            get {
                return ((string)(this["AutoDim_Y"]));
            }
            set {
                this["AutoDim_Y"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("_Z")]
        public string AutoDim_Z {
            get {
                return ((string)(this["AutoDim_Z"]));
            }
            set {
                this["AutoDim_Z"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool PropertyWizard_ValidateProperties {
            get {
                return ((bool)(this["PropertyWizard_ValidateProperties"]));
            }
            set {
                this["PropertyWizard_ValidateProperties"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool PropertyWizard_FixPartNum {
            get {
                return ((bool)(this["PropertyWizard_FixPartNum"]));
            }
            set {
                this["PropertyWizard_FixPartNum"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool ShowErrors {
            get {
                return ((bool)(this["ShowErrors"]));
            }
            set {
                this["ShowErrors"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool ShowErrorsThisSession {
            get {
                return ((bool)(this["ShowErrorsThisSession"]));
            }
            set {
                this["ShowErrorsThisSession"] = value;
            }
        }
    }
}
